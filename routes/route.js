var pg = require('pg');

var morgan = require('morgan');
var mysql = require('mysql');
var path = require('path');
var cors = require('cors');
var dateFormat = require('dateformat');

var port = process.env.PORT || 8090;
// const errorLog = require('./logs/logger').errorlog;
// const successlog = require('./logs/logger').successlog;



// var config = {
//     user: CONSTANT.DB_USERNAME,
//     database: CONSTANT.DB_SCHEMA,
//     host: CONSTANT.DB_HOST,
//     password: process.env.PGPASSWORD,
//     port: CONSTANT.DB_PORT
// };


var pool_pg = {
    user: 'root',
    database: 'platform3',
    host: 'prod-db-01.ec2farm.platform3.co',
    password: 'Z?W5B#Qw!qX3VQy',
    port: '5432'
};
console.log("Pool_pg : " + JSON.stringify(pool_pg));

var pool_mysql = mysql.createPool({
    host: 'mysql-prod-db.ec2farm.platform3.co',
    port: '3306',
    user: 'webadmin',
    password: 'L5&NKQ!Nqx$JN',
    database: 'universal_rpt'
})

var port = process.env.PORT || 8090;

function handleDisconnect(conn) {
    conn.on('error', function(err) {
        if (!err.fatal) {
            return;
        }

        if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
            throw err;
        }

        console.log('Re-connecting lost connection: ' + err.stack);

        connection = mysql.createConnection(conn.config);
        handleDisconnect(connection);
        connection.connect();
    });

}

handleDisconnect(pool_mysql);

var pool_pg = new pg.Pool(pool_pg);

const getPendingReceipts = (request, response) => {
    var pgQuery;
    pgQuery =
        "select s.id, s.first_name, count( * ) as total,  " +
        "COUNT(CASE WHEN status = 'new'THEN 1 END) new, " +
        // "COUNT(CASE WHEN status = 'approved' THEN 1 END) approved, " +
        "COUNT(CASE WHEN status = 'duplicate' THEN 1 END) duplicate, " +
        // "COUNT(CASE WHEN status = 'rejected'THEN 1 END) rejected, " +
        "COUNT(CASE WHEN status = 'pending' THEN 1 END) pending  " +
        "from snap3_client s right join receipt_details r " +
        "on s.id = r.client_id where r.date > '2020-01-01' and r.is_active='true' " +
        " and s.first_name<>'' " +
        "group by s.id, r.client_id, s.first_name " +
        "order by s.first_name asc"

    pool_pg.query(pgQuery, function(err, results) {
        if (err) {
            throw err
        }
        // response.status(200).json(results.rows)
        var jsonData = results.rows;
        var receiptData = [];
        var mydata = [];
        for (i = 0; i < jsonData.length; i++) {

            receiptData.push('', jsonData[i].id, jsonData[i].first_name, jsonData[i].new, jsonData[i].duplicate, jsonData[i].pending);


            if ((jsonData[i].new > 0) || (jsonData[i].duplicate > 0) || (jsonData[i].pending > 0)) {
                // mydata.push(jsonData[i].first_name, jsonData[i].new, jsonData[i].duplicate, jsonData[i].pending)

                mydata.push({
                    first_name: jsonData[i].first_name,
                    new: jsonData[i].new,
                    duplicate: jsonData[i].duplicate,
                    pending: jsonData[i].pending
                });
            }
        }
        // console.log('inside', mydata)
        response.render('pages/index', { data: mydata })
            // response.render('pages/index');

    });
}

module.exports = {
    getPendingReceipts
}