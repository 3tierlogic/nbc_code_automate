const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./routes/route')
const port = 3000
var favicon = require('serve-favicon')
var tablesort = require('tablesort');

// tablesort(el, options);
// following required for ejs
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')));
// Set 'views' directory for any views 
app.set('views', path.join(__dirname, 'views'));
app.set('public', path.join(__dirname, 'pulic'));
// view engine setup
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', db.getPendingReceipts)

app.get('/receipts', db.getPendingReceipts)
    // response.render('pages/index');
    // app.get('/users/:id', db.getUserById)
    // app.post('/users', db.createUser)
    // app.put('/users/:id', db.updateUser)

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})